# Projet WEB deuxième année

### Consignes : 
    

    - Le site doit permettre :
        • aux groupes de créer un compte, soumettre les données et les consulter
        • aux responsables de voir la liste des candidatures (résumé), et le détail de chaque candidature

    - Lien du sujet : 
    
        •https://extra.u-picardie.fr/moodle/upjv/pluginfile.php/471030/mod_resource/content/0/Projet-Festival.pdf
